---
weight: 300
title: WlxOverlay
---

# WlxOverlay

WlxOverlay is a tool that lets users interact with their X11 or Wayland desktop from inside VR.

It supports a vast variety of desktop environments, and comes with a fully customizable keyboard.

There are currently two editions of WlxOverlay.

## WlxOverlay-S 

- [WlxOverlay-S GitHub repository](https://github.com/galister/wlx-overlay-s)

WlxOverlay-S is the newer iteration of the software with major performance improvements. We recommend trying this first.

The S edition supports both Monado and SteamVR, and avoids many of the pitfalls of the previous iterations by using Vulkan instead of EGL.

## WlxOverlay Classic

- [WlxOverlay GitHub repository](https://github.com/galister/WlxOverlay)
- [Getting Started Wiki](https://github.com/galister/WlxOverlay/wiki/Getting-Started)
- [Troubleshooting Wiki](https://github.com/galister/WlxOverlay/wiki/Troubleshooting)

WlxOverlay Classic is recommended if WlxOverlay-S is not working for you. 

It works well once you get it working, but the lengthy troubleshooting page speaks for itself. 

It is prone to run into issues with NVidia's EGL driver, as well as GLFW specifically.

## Support

Reach out in the `wlxoverlay` room in Discord or Matrix.